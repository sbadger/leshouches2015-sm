#!/bin/sh

rm LH15.bib
touch LH15.bib

for sec in SM_*
do
  ls $sec/*.bib
  cat $sec/*.bib >> LH15.bib
done
