\section{On the accuracy and Gaussianity of the PDF4LHC15
    combined sets of parton distributions
    \texorpdfstring{\protect\footnote{
      S.~Carrazza, S.~Forte, Z.~Kassabov, J.~Rojo
    }}{}}

We perform a systematic study of the combined PDF4LHC15 sets of parton
distributions which have been recently presented as a means to
implement the PDF4LHC prescriptions for the LHC Run II.
%
These combined sets
reproduce a prior large Monte Carlo (MC) sample in terms of
either a smaller  MC replica sample, or a Gaussian (Hessian)
representation with two different number of error sets, and obtained
using two different methodologies.
%
We study how well the three reduced sets reproduce the prior
for all the $N_{\sigma}\simeq 600$ hadronic
cross-sections used in the NNPDF3.0 fit.
%
We  then 
quantify deviations of the prior set from  Gaussianity, and
check that these deviations are well reproduced by the MC reduced set.
%
Our results indicate that generally the three reduced sets perform reasonably well,
and provide some guidance about which of these to use in specific applications.

\subsection{Introduction}
Recently, the PDF4LHC Working Group~\cite{PDF4LHC} has presented 
updated recommendations and guidelines~\cite{Butterworth:2015oua}
for the usage of Parton Distribution
Functions (PDFs) for LHC calculations at Run II.
%
These recommendations are specifically based on the usage of combined
PDF sets, which are obtained using
the  Monte Carlo (MC) method~\cite{Watt:2012tq,Forte:2013wc},
constructed
from the combination of $N_{\rm rep}=300$ MC replicas 
from the NNPDF3.0~\cite{Ball:2014uwa},
MMHT2014~\cite{Harland-Lang:2014zoa} and CT14~\cite{Dulat:2015mca} PDF sets,
for a total number of $N_{\rm rep}=900$ replicas.
%
The combination has been performed both at NLO and at NNLO, and
versions with $n_f=4$ and $n_f=5$ maximum number of active
quark flavors are available.
%
The impact of LHC measurements
from Run I on PDF determinations has been discussed in a companion
PDF4LHC report~\cite{Rojo:2015acz}. 

From this starting prior set,
three reduced sets, two Hessian and one MC, are delivered for general
usage.
%
The reduced sets, constructed using different compression
strategies, are supposed to reproduce as much as possible
the information
contained in the prior, but in terms of a substantially smaller number of error PDFs.
%
The reduced Monte Carlo set, {\tt PDF4LHC15\_mc}, is
constructed using the CMC-PDF method~\cite{Carrazza:2015hva},
and the two reduced Hessian sets,
{\tt PDF4LHC15\_100} and {\tt PDF4LHC15\_30}, are constructed using the
MC2H~\cite{Carrazza:2015aoa} and META-PDF~\cite{Gao:2013bia} techniques, respectively.
%
The PDF4LHC15 combined sets are available from {\tt LHAPDF6}~\cite{Buckley:2014ana},
and include additional PDF member sets to account for the uncertainty due to
the value of the strong coupling constant,
$\alpha_s(m_Z)=0.1180\pm 0.0015$.

The PDF4LHC 2015 report~\cite{Butterworth:2015oua} presented general
guidelines for the usage of the reduced sets, and some comparisons
 between them and the prior 
at the level of PDFs, parton luminosities, and LHC cross-sections,
while referring to a repository of cross-sections on the PDF4LHC
server~\cite{PDF4LHCrep} for a more detailed set of comparisons.
It is the purpose of this contribution to make these comparisons more
systematic and quantitative, in order to answer questions which have
been frequently asked on the usage of the reduced sets.
Specifically, we  will perform a systematic
 study of the accuracy of the PDF4LHC15 reduced sets, by assessing
 the relative accuracy of uncertainties determined using each of them
 instead of the prior, for all hadronic observables
 included in the NNPDF3.0
 PDF determination~\cite{Ball:2014uwa}. We will also compare the
 performance of the PDF4LHC15 reduced sets with that of the recently
 proposed SM-PDF sets~\cite{Carrazza:2016htc}: specialized PDF sets
 which strive to minimize the number of PDF error sets which are
 needed for the description of a particular class of processes.
%
 We will then
 address the issue of the validity of the Gaussian approximation to
 PDF uncertainties by testing for gaussianity of the distribution of
 results obtained using the prior PDF set for a very wide variety of
 observables, and then assessing the performance and accuracy of  
both the Monte Carlo sets (which
 allows for non-Gaussian behaviour) and the Hessian compressed sets
 (which do not, by construction).

\subsection{Validation of the PDF4LHC15 reduced PDF sets on a global dataset}
\label{sec:pdf2:validation}

%
We wish to compare the performance of the three
reduced NLO sets, the two Hessian
sets, {\tt PDF4LHC15\_nlo\_30} and {\tt PDF4LHC15\_nlo\_100}, and the Monte
Carlo set {\tt PDF4LHC15\_nlo\_mc}, for all the hadronic 
cross-sections included
in the NNPDF3.0 global analysis~\cite{Ball:2014uwa}.
%
These cross-sections have computed at $\sqrt{s}=7$ TeV using NLO theory with
{\tt MCFM}~\cite{Campbell:2004ch},
{\tt NLOjet++}~\cite{Nagy:2001fj}
and {\tt aMC@NLO}~\cite{Alwall:2014hca,Bertone:2014zva}
interfaced to {\tt APPLgrid}~\cite{Carli:2010rw}.
%
They include
 $N_{\sigma}\simeq 600$ independent observables for
a variety of hadron collider processes such as
 electroweak gauge boson, jet
production and top quark pair production, covering
a wide region in the $(x,Q)$ kinematical plane.
%
In this calculation, the PDF4LHC15 combined sets are
obtained from the {\tt LHAPDF6}
interface.


%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
  \includegraphics[width=0.49\textwidth]{SM_PDF2/plots/meansnlo}
  \includegraphics[width=0.49\textwidth]{SM_PDF2/plots/sigmanlo}
\end{center}
\vspace{-0.5cm}
\caption{small 
  \label{fig:ratios}
  Distribution of the ratio to the prior of means Eq.~(\ref{eq:rat1}) (left) and of standard deviations
  Eq.~(\ref{eq:rat2}) (right) computed for each of the  $N_{\sigma}\simeq 600$
  hadronic cross-sections
included in the NNPDF3.0 global
analysis, using each of  the three reduced sets. 
}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centering
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nlo_30_nlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nlo_30_Worst-10-nlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nlo_100_nlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nlo_100_Worst-10-nlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nlo_mc_nlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nlo_mc_Worst-10-nlo}\\
  \includegraphics[width=.6\textwidth]{SM_PDF2/plots/legend.pdf}
  \caption{\small 
    Relative difference Eq.~(\ref{eq:reldiff}),
    between the PDF uncertainties computed using the reduced set and
    the prior
    computed for all hadronic observables included
    in the NNPDF3.0 fit, shown as a scatter plot in
    the $(x,Q^2)$ at the corresponding point, determined
    using leading-order kinematics. From top to bottom results for
    {\tt PDF4LHC\_nlo\_30}, 
    {\tt PDF4LHC\_nlo\_100} and {\tt PDF4LHC\_nlo\_mc} are shown. In
    the left plots, all points are shown, in the right plots only the
    10\% of points with maximal deviation.
   \label{fig:kinplots}}
\end{figure}
%%%%%%%%%%%%%%%%%%%%


In Fig.~\ref{fig:ratios} we show the distribution of the ratios
\bea
\label{eq:rat1}
R_{\la \sigma_i\ra} &\equiv& \frac{\la \sigma_i\ra({\rm reduced})}{\la \sigma_i\ra({\rm prior})} \,,\quad i=1\,\ldots,N_{\sigma} \, , \\ 
R_{s_i} &\equiv& \frac{s_i({\rm reduced})}{s_i({\rm prior})} \, , \quad i=1\,\ldots,N_{\sigma} \, .
\label{eq:rat2}
\eea
between respectively the  
means $\la \sigma_i\ra$, 
and the  standard
deviations $s_i$  from each of the three reduced sets and the PDF4LHC15
prior, computed for all hadronic observables included in the NNPDF3.0
global analysis.
%
For the Hessian sets the central value coincides with that of the
prior, so the  ratio of means is supposed to equal one by
construction, with small deviations only due to rounding errors and
interpolation in the construction of the {\tt LHAPDF} grids, while for the
MC set the mean is optimized by the CMC construction to fluctuate due
to the finite size of the replica sample much less than expected on
purely statistical grounds.
%s
Indeed, the histograms shows agreement of
central values at
the permille level.
For standard deviations  (i.e. PDF uncertainties)
Fig.~\ref{fig:ratios} shows that using 
the {\tt
  PDF4LHC15\_nlo\_100} set
 they are reproduced typically with better than  5\% accuracy. 
%
Differences are
somewhat larger for the {\tt PDF4LHC15\_nlo\_mc} and
{\tt PDF4LHC15\_nlo\_30} sets.


%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centering
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nnlo_30_nnlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nnlo_30_Worst-10-nnlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nnlo_100_nnlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nnlo_100_Worst-10-nnlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nnlo_mc_nnlo}
  \includegraphics[width=0.45\textwidth]{SM_PDF2/plots/std_distPDF4LHC15_nnlo_mc_Worst-10-nnlo}
  \caption{\small Same as Fig.~\ref{fig:kinplots}, but using the NNLO
    PDF sets.
   \label{fig:kinplots2}}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

In order to investigate the accuracy of PDF uncertainties in a more
detailed quantitative way, we define 
the relative
difference between the standard deviation, $s_i^{\rm (red)}$, of the cross-section $\sigma_i$
computed with the reduced sets, and  that of the prior,
$s^{\rm (prior)}_i$:
\begin{equation}
	\Delta_i \equiv \frac{\left| s^{\rm (prior)}_i  - s_i^{\rm (red)} \right|}{
	  s^{\rm (prior)}_i}
        \label{eq:reldiff}\, , \quad i=1,\ldots,N_{\sigma} \, .
\end{equation}
In Figs.~\ref{fig:kinplots} and~\ref{fig:kinplots2} 
the relative differences $\Delta_i$ are shown using NLO and NNLO PDFs 
for all hadronic observables which enter the NNPDF3.0 fit as a scatter plot in
the $(x,Q^2)$ kinematic plane, at the point corresponding to each
observable using leading order kinematics~\cite{Ball:2010de},
both for
all observables, and for the 10\% of observables
exhibiting the largest relative differences.
%
The $x$ value
    corresponding to the the parton with highest $x$ is always
    plotted, and for one-jet inclusive cross-sections, for which the
    $x$ values of the two partons are not fixed even at leading order,
    the largest accessible $x$ which corresponds to the rapidity range
    of each data point is plotted.
%
    Of course, these $x$ values should
    be only taken as indicative, and it should be born in mind that
    for most of the processes considered when one of the two partons
    involved is at large $x$, the other is at rather smaller $x$.
    %
In this comparison, NLO theory is used
throughout.

From Figs.~\ref{fig:kinplots} and~\ref{fig:kinplots2}  we see that
    for {\tt PDF4LHC15\_nlo\_100} deviations are generally small, and
concentrated in regions in which  experimental information is scarce
and PDF uncertainties are largest,
such as the region of large $x$ and large $Q$.
%
For {\tt PDF4LHC15\_nlo\_mc} and {\tt PDF4LHC15\_nlo\_30} the
deviations are somewhat larger but still moderate in most
cases, with a few outliers. No significant difference is observed between NLO
and NNLO, consistent with the expectation that PDF uncertainties are
driven by data, not by theory, and thus are very similar at NLO and NNLO.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}[t]
\footnotesize
\begin{centering}
  \begin{tabular}{|c|c|c|c|}
    \hline
process & distribution  &  $N_{{\rm bins}}$ & range  \tabularnewline
\hline 
\hline 
\multirow{2}{*}{$gg\to h$} &  $d\sigma/dp_t^h$  & 10 & {[}0,200{]} GeV\tabularnewline
 & $d\sigma/dy^h$ & 10 & {[}-2.5,2.5{]} \tabularnewline
\hline 
\multirow{2}{*}{VBF $hjj$} &  $d\sigma/dp_t^h$  & 5 & {[}0,200{]} GeV \tabularnewline
& $d\sigma/dy^h$ &  5 & {[}-2.5,2.5{]} \tabularnewline
\hline 
\multirow{2}{*}{$hW$}&  $d\sigma/dp_t^h$  & 10 & {[}0,200{]} GeV \tabularnewline
 & $d\sigma/dy^h$ &  10 & {[}-2.5,2.5{]} \tabularnewline
\hline 
\multirow{2}{*}{$hZ$} & $d\sigma/dp_t^h$  & 10 & {[}0,200{]} GeV \tabularnewline
& $d\sigma/dy^h$ &  10 & {[}-2.5,2.5{]} \tabularnewline
\hline
\multirow{2}{*}{$ht\bar{t}$} &  $d\sigma/dp_t^h$  & 10 & {[}0,200{]} GeV \tabularnewline
 & $d\sigma/dy^h$ &  10 & {[}-2.5,2.5{]} \tabularnewline
\hline
\end{tabular}$\quad $\begin{tabular}{|c|c|c|c|}
    \hline
process & distribution  &  $N_{{\rm bins}}$ & range  \tabularnewline
\hline 
\hline
\multirow{8}{*}{$Z$} & $d\sigma/dp_t^{l^-}$ &  10 & {[}0,200{]} GeV \tabularnewline
 & $d\sigma/dy^{l^-}$ &  10 & {[}-2.5,2.5{]} \tabularnewline
 & $d\sigma/dp_t^{l^+}$ &  10 & {[}0,200{]} GeV \tabularnewline
 & $d\sigma/dy^{l^-}$ &  10 & {[}-2.5,2.5{]} \tabularnewline
 & $d\sigma/dp_t^{Z}$ &  10 & {[}0,200{]} GeV \tabularnewline
 &  $d\sigma/dy^{Z}$&  5 & {[}-4,4{]} \tabularnewline
 & $d\sigma/dm^{ll}$&  10 & {[}50,130{]} GeV \tabularnewline
& $d\sigma/dp_t^{ll}$ &  10 & {[}0,200{]} GeV \tabularnewline
\hline
  \end{tabular}\\[0.2cm]
   \begin{tabular}{|c|c|c|c|}
    \hline
process & distribution  &  $N_{{\rm bins}}$ & range  \tabularnewline
\hline 
\hline 
\multirow{7}{*}{$t\bar{t}$}  &  $d\sigma/dp_t^{\bar{t}}$  &  10 & {[}40,400{]} GeV \tabularnewline
 & $d\sigma/dy^{\bar{t}}$ &  10 & {[}-2.5,2.5{]} \tabularnewline
 & $d\sigma/dp_t^{t}$ &  10 & {[}40,400{]} GeV \tabularnewline
 & $d\sigma/dy^{t}$&  10 & {[}-2.5,2.5{]} \tabularnewline
 & $d\sigma/dm^{t\bar{t}}$ &  10 & {[}300,1000{]} \tabularnewline
 & $d\sigma/dp_t^{t\bar{t}}$  & 10 & {[}20,200{]} \tabularnewline
 & $d\sigma/dy^{t\bar{t}}$ &  12 & {[}-3,3{]}  \tabularnewline
\hline
\end{tabular}$\quad $\begin{tabular}{|c|c|c|c|}
    \hline
process & distribution  &  $N_{{\rm bins}}$ & range  \tabularnewline
\hline 
\hline
\multirow{7}{*}{$W$} &   $d\sigma/d\phi$ &  10 & {[}0,200{]} GeV \tabularnewline
 & $d\sigma/dE_t^{\rm miss}$ &  10 & {[}-2.5,2.5{]} \tabularnewline
 & $d\sigma/dp_t^{l}$ &  10 & {[}0,200{]} GeV \tabularnewline
 & $d\sigma/dy^{l}$ &  10 & {[}-2.5,2.5{]} \tabularnewline
 & $d\sigma/dm_t$ &  10 & {[}0,200{]} GeV \tabularnewline
 &  $d\sigma/dp_T^{W}$&  5 & {[}-4,4{]} \tabularnewline
 & $d\sigma/y^{W}$&  10 & {[}50,130{]} GeV \tabularnewline
\hline
\end{tabular}
\par\end{centering}
\caption{\small LHC processes and the corresponding 
  differential distributions
  used as input in the construction of the
  {\tt SM-PDF-Ladder} set.
  %
  In each case we indicate the range
  spanned by each distribution and the number of
  bins $N_{\rm bins}$.
  %
  All processes have been computed for  $\sqrt{s}=13$ TeV.
  %
  Higgs bosons and top quarks are stable, while weak gauge bosons are assumed
  to decay leptonically.
  %
  No acceptance cuts are imposed with the exception of the leptons from
  the gauge boson decay, for which we require
  $p^l_{T}\geq10$ GeV and  $|\eta^{l}|\leq2.5$.
  \label{tab:processes_H}
}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This exercise shows that about $N_{\rm eig}=100$ Hessian eigenvectors are necessary
for a good accuracy general-purpose PDF set.
%
On the other hand, 
we have  recently argued~\cite{Carrazza:2016htc}  that
a much smaller set of Hessian eigenvectors is sufficient in order to
accurately reproduce a subset of cross-sections, and presented a
technique to construct such specialized minimal sets, dubbed SM-PDFs.
%
In order to test and
validate this claim, we have constructed two such SM-PDF sets, using
the methodology of Ref.~\cite{Carrazza:2016htc}, and starting
from the  PDF4LHC15 NLO prior:
\begin{itemize}
\item {\tt SM-PDF-ggh}: this SM-PDF set, with
  $N_{\rm eig}=4$
  symmetric eigenvectors, reproduces the inclusive cross-section
  and the $p_T$ and rapidity distributions of Higgs production
  in gluon fusion at $\sqrt{s}=13$ TeV.
  %
\item {\tt SM-PDF-Ladder}: this SM-PDF set, with has $N_{\rm eig}=17$
  symmetric eigenvectors, reproduces all the observables listed in 
  Table~\ref{tab:processes_H}, which include a wide variety of LHC processes
  at $\sqrt{s}=13$ TeV.
  %
\end{itemize}
The {\tt APPLgrid} grids
for the processes in Table~\ref{tab:processes_H}
have been  computed using {\tt aMC@NLO} interfaced
to {\tt aMCfast}.

%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
  \includegraphics[width=0.49\textwidth]{SM_PDF2/plots/std_distSMPDF-ggH_nlo}
  \includegraphics[width=0.49\textwidth]{SM_PDF2/plots/std_distSMPDF-ggH_Worst-10-nlo}
  \includegraphics[width=0.49\textwidth]{SM_PDF2/plots/std_distSMPDF-Ladder_nlo}
  \includegraphics[width=0.49\textwidth]{SM_PDF2/plots/std_distSMPDF-Ladder_Worst-10-nlo}
\end{center}
\vspace{-0.6cm}
\caption{\small Same as Fig.~\ref{fig:kinplots}, this time for the two SM-PDF
  sets, {\tt SM-PDF-ggh} (upper plots) and {\tt SM-PDF-Ladder}
  (lower plots).
  \label{fig:kinplots3}}
\end{figure}
%%%%%%%%%%%%%%%%%%%%


In Fig.~\ref{fig:kinplots3} we show again the relative difference
	$\Delta_i$ Eq.~(\ref{eq:reldiff}), which was shown in
Fig.~\ref{fig:kinplots}, but now comparing to the prior these two SM-PDF sets.
In the case of the {\tt SM-PDF-ggh} set,
we find good agreement with the prior
for all cross-sections on the region $x\simeq 0.01$ and
$Q\simeq 100$ GeV, relevant for Higgs production in gluon fusion.
%
On the other hand, as we move outside this region,
the accuracy rapidly deteriorates. This  exemplifies the virtues 
and limitations of the SM-PDF approach: a very small number of
eigenvectors is sufficient to reproduce a reasonably small set of
observables,  but if one tries to stretch results to too many
processes there is accuracy loss.
%
The {\tt SM-PDF-Ladder} set, on the other hand,
exhibits a similar performance
as the
{\tt
  PDF4LHC15\_nlo\_30} set.

\subsection{Non-Gaussianities in the PDF4LHC combination}

As discussed in the PDF4LHC15 report~\cite{Butterworth:2015oua},
the Monte Carlo combination of individual PDF set  in general is not
Gaussian. This is both because one of the three sets entering the
combination, NNPDF3.0, allows for non-Gaussian behaviour, and also
because in general the combination of Gaussian sets is not necessarily
Gaussian itself. 
%
We will now study in a more systematic way the degree of non-Gaussianity
of the prior set, and specifically correlate the comparison of the
reduced sets to the prior with  the degree of non-Gaussianity of the
prior. This has the threefold purpose of determining how much the
accuracy of the Hessian set deteriorates in the presence of
non-Gaussianities, of checking that the reduced MC set correctly
reproduces the non-Gaussianity of the prior, and of providing guidance
on when the MC set should be favored over the Hessian sets in order to
reproduce the non-Gaussianity.
\clearpage


In order to study non-Gaussianity, we proceed in two steps. First, we
turn the histogram, as
obtained from a Monte Carlo representation, into a continuous
probability distribution. Then, we compare this probability
distribution to a Gaussian with the same mean and standard deviation.
The first step is accomplished using the
Kernel
Density Estimate (KDE) method. The second, using the Kullback–Leibler
(KL) divergence as a measure of the difference between two probability
distributions (for a brief review of
both methods see e.g. Ref.~\cite{Bishop:1995}).

%
The KDE method consists of constructing the probability distribution
corresponding to a histogram as the
average of kernel
functions $K$ centered at the data from which the histogram would be constructed. In our case, given $k=1,\dots,N_{\rm
  rep}$ replicas of the $i$-th cross-section
 $\{\sigma_i^{(k)}\}$, the probability distribution is
\begin{equation}
  P(\sigma_i)=\frac{1}{N_{\rm rep}}\sum_{k=1}^{N_{\rm rep}}K\left(\sigma_i-\sigma_{i}^{(k)}\right)\,,\quad
  i=1,\ldots,N_{\sigma} \, .\label{eq:KDE}
\end{equation}
%
We specifically choose 
\begin{equation}
K(\sigma-\sigma_{i})\equiv \frac{1}{h\sqrt{2\pi}}\exp\lp -\frac{(\sigma-\sigma_{i})^{2}}{h}\rp\,,\label{eq:kdenormal}
\end{equation}
where the parameter $h$, known as  bandwidth, is 
 \begin{equation}
   h = \hat{s}_i \left( \frac{4}{3N_{\rm rep}}  \right)^\frac{1}{5} \, ,
 \end{equation}
 where $\hat{s}_i$ is the standard deviation of the given sample of
 replicas. This choice is known as {\it Silverman rule}, and, if the
 underlying probability distribution is Gaussian, it minimizes
 the integral of the square difference between the ensuing distribution and
 this underlying Gaussian~\cite{Silverman:1998}.
 %
Once turned into continuous distributions via the KDE method, the
prior and reduced Monte Carlo sets can be compared to each other, to a
Gaussian, and to the Hessian sets. The comparison can be performed
using the  Kullback–Leibler (KL) divergence, which measures the
information loss when using
a probability
distribution  $Q(x)$ to approximate a  prior $P(x)$, and
is given by
\begin{equation}
  \label{eq:kldiv}
D_{\rm KL}^{(i)}(P|Q)=\int_{-\infty}^{+\infty}\lp P(x)\cdot \frac{\log
P(x)}{\log Q(x)} \rp dx. \ ,
\end{equation}

%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
  \includegraphics[width=0.75\textwidth]{plots/cmcgood}
\end{center}
\vspace{-0.3cm}
\caption{\small The probability distribution for 
the most forward bin
  in the LHCb $Z\to \mu\mu$ 8 TeV measurement obtained using the
  PDF4LHC15 prior and the  {\tt PDF4LHC15\_nlo\_mc} and
  {\tt PDF4LHC15\_nlo\_100} reduced sets.
 The value of the KL divergence $D_{\rm KL}$
  Eq.~(\ref{eq:kldiv}) between the prior and a Gaussian, and between 
each of the two reduced sets and the
  prior for this distribution, are also given.
\label{fig:example1}
}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

As a first example,
in Fig.~\ref{fig:example1} we select a data bin in which the
distribution of PDF replicas is clearly non-Gaussian,  namely the
most forward rapidity bin
  in the LHCb $Z\to \mu\mu$ 8 TeV measurement~\cite{Aaij:2015vua}, and
  we compare the distribution obtained  using the
  PDF4LHC15 prior to those found using 
the 
  {\tt PDF4LHC15\_nlo\_mc} and
  {\tt PDF4LHC15\_nlo\_100} reduced sets. The continuous distribution
  shown is obtained from the 
prior and reduced MC samples using the KDE method discussed above. 
For the {\tt PDF4LHC15\_nlo\_100} set the distribution shown is a
Gaussian with with and central value using the standard procedure,
based on linear error propagation, which is used to obtain predictions
from Hessian sets: namely, the central set provides the mean, and the
standard deviation is the sum in quadrature of the deviations obtained
using each of the error sets. 

The KL divergence between the prior and a Gaussian is equal to
 $D_{KL}=0.153$, while the divergence between the prior
 and
its reduced MC representation is $D_{KL}=0.055$, and finally between the
prior and Hessian set it is
$D_{KL}=0.19$. This shows that the reduced MC representation of the
prior is much closer to it than the prior is to a Gaussian, while the
Hessian representation differs from it even more.
In order to facilitate the
interpretation these values of the KL
divergence, in Fig.~\ref{fig:example2} we plot the value of the KL
divergence between two Gaussian with different width, as a function
of the ratio of their width: the plot shows that  $D_{KL}\sim0.05$
corresponds to distorting the width of a Gaussian by about 20\%.
%
In this figure we also show as horizontal lines the minimum and
maximum values that we obtained, as well as the edges of the four
quartiles of the distribution of results.

%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
 \includegraphics[width=0.75\textwidth]{plots/kl_absval}
\end{center}
\vspace{-0.3cm}
\caption{\small The
   KL divergence $D_{KL}$ Eq.~(\ref{eq:kldiv})  between two Gaussian distributions
  with the same mean but different widths, as a function of the ratio
  of their standard deviations.
  %
We also show (horizontal lines) 
the highest value, lowest value, and the edges of the quartiles of
the distribution of $D_{KL}$ values between the prior and a Gaussian
approximation to it, for all observables listed in
Table~\ref{tab:processes_H}.
 \label{fig:example2}
}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

We have extended the type of comparisons shown in
Fig.~\ref{fig:example1} into a systematic study
including
all the cross-sections listed in 
Table~\ref{tab:processes_H}.
%
As discussed in Sect.~\ref{sec:pdf2:validation},
this is a reasonably representative set of observables, since
it is possible to construct a PDF set, the {\tt SM-PDF-Ladder}, which is adequate to
describe  them and is also accurate to describe
all the hadronic cross-section from the NNPDF3.0 fit (see Fig.~\ref{fig:kinplots3}).
%
Specifically, for each cross-section
we have determined the probability distribution from the prior using
the KDE method Eq.~(\ref{eq:KDE}),
and also a Gaussian approximation to it, defined as
the Gaussian with the same mean and standard deviation as the
prior.
%
We have computed the KL divergence between the prior
distribution and this Gaussian approximation.
%
It is clear that the vast
majority of observables exhibits Gaussian behaviour to good
approximation, with extreme cases such as shown in
Fig.~\ref{fig:example1} happening in a small fraction of the first
quartile. 
%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.75\textwidth]{plots/kl_all}
\end{center}
\vspace{-0.3cm}
\caption{\small 
  \label{fig:kl_all} The KL divergence, Eq.~(\ref{eq:kldiv}) between
  the prior and each of its two reduced representations
  {\tt PDF4LHC15\_nlo\_prior} (Monte Carlo) and 
  {\tt PDF4LHC15\_nlo\_mc} (Hessian) vs. the divergence between the
  prior and its Gaussian approximation, computed for all observables
  listed  in Table~\ref{tab:processes_H}.
}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

We have then computed for each observable the KL distance between
the prior and the  {\tt PDF4LHC15\_nlo\_mc} and  {\tt
  PDF4LHC15\_nlo\_100} combined sets.
%
Results are collected in
  Fig.~\ref{fig:kl_all} for all processes, while in
  Fig.~\ref{fig:kl_allbreak} we show a breakdown for the four classes
  of processes of Table~\ref{tab:processes_H}: Higgs, top, $W$ and $Z$ production.
  %
For each cross-section there are two points on the plot, one
  corresponding to {\tt PDF4LHC15\_nlo\_mc}  and the other to {\tt
  PDF4LHC15\_nlo\_100}. The points are plotted with  on the $x$ axis the KL
  divergence between the prior and its gaussian approximation, and on
  the $y$ axis the same quantity now evaluated
  between the prior and the compressed set.
  %
For the {\tt
  PDF4LHC15\_nlo\_100} all points cluster on the diagonal: this means that
the reduced Hessian set only deviates from the prior inasmuch as the
prior deviates from a Gaussian --- only for a more extreme deviation
from Gaussian such as shown in Fig.~\ref{fig:example1} does the
reduced Hessian deviate more. 
The  {\tt PDF4LHC15\_nlo\_mc} points
instead approximately fall within   a horizontal band: this means that the
quality of the approximation to the prior of the
reduced MC does not depend on the
degree of non-Gaussianity of the prior itself. 

Hence, the reduced MC
set does reproduce well the non-Gaussian features of the prior, when
they are present, and it will be advantageous to use it  
for points where the center
of the band corresponding to  {\tt PDF4LHC15\_nlo\_mc} is below the
diagonal. Figure~\ref{fig:kl_allbreak}   shows that this happens
for a significant fraction of the $W$ and $Z$ production
cross-sections, but not
for top and Higgs production.
%
This is consistent with the expectation that
non-Gaussian behaviour is mostly to be found in large $x$ PDFs, which
are probed by gauge boson production at high rapidity, but not by
Higgs and top production which are mostly sensitive to the gluon PDF at
medium and small $x$.
%

%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.45\textwidth]{SM_PDF2/plots/kl_higgs}
\includegraphics[width=0.45\textwidth]{SM_PDF2/plots/kl_ttbar}\\
\includegraphics[width=0.45\textwidth]{SM_PDF2/plots/kl_wprod}
\includegraphics[width=0.45\textwidth]{SM_PDF2/plots/kl_zprod}
\end{center}
\vspace{-0.3cm}
\caption{\small 
  \label{fig:kl_allbreak} Same as Fig.~\ref{fig:kl_all}, now separating
  the contributions of the different classes of processes of
  Table~\ref{tab:processes_H}: Higgs production (top left), top quark
  pair production (top right), $W$
  production (bottom left) and $Z$ production (bottom right).
}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

In order to further elucidate the dominant  non-Gaussian features, we
have  performed a comparison of 
the mean and the standard deviation of each probability
distribution with respectively the median and the 
minimum 68\% confidence interval $R$, defined as
%
\begin{equation}
R=\frac{1}{2}\min\{[x_{\min},x_{\max}];\qquad
\int_{x_{\min}}^{x_{\max}} P(x) =0.683 \, .
\end{equation}
The deviation of the median from the mean is a measure of the
asymmetry of the distribution, while the deviation  $R$ from the
standard deviation is a measure of the presence of outliers.
%
We then define two estimators, one for the deviation of the mean from the
median and for the deviation of the standard deviation $s$ from $R$:
%
\begin{equation}
	\label{eq:meanshift}
	\Delta_{\mu} = \frac{\textrm{median} - \mu}{s} \, ,
\end{equation}
\begin{equation}
	\label{eq:stdshift}
	\Delta_{s} = \frac{R - s}{s} \, .
\end{equation}
%
Both $\Delta_{\mu}$ and $\Delta_s$ would vanish for a Gaussian in the
limit of infinite sample size.

In Fig.~\ref{fig:estimators} we represent these two
estimators in a scatter plot, with $\Delta_{\mu}$ and
$\Delta_s$ respectively on the $x$ and $y$ axis,
computed for all the cross-sections of Table~\ref{tab:processes_H}.
%
In addition,
we show a
color code with the KL
divergence between the prior and respectively its Gaussian
approximation and its two reduced MC and Hessian representations.
%
From this comparison, it is clear that  the shift in the median 
is  only weakly correlated to the degree of non-Gaussianity (top
plot), and also weakly correlated to the shift is standard deviation,
which instead is strongly correlated to non-Gaussianity.

In the presence of outliers, $R\le s$, and indeed $R$ is seen to be
always negative.
%
We expect asymmetries related to non-Gaussian
behaviour to be due to the fact that in some cases PDFs are bounded
from below by positivity, but not from above where outliers may be
present.
%
Indeed in the non-gaussian region $\Delta_{\mu}$ tends to be
negative, but with large fluctuations in its value.
The same correlations are seen with the KL divergence between prior
and Hessian, again showing that this is dominated by non-gaussian
behaviour.
%
On the other hand, no correlation is observed from the divergence between
prior and reduced MC, consistent with our conclusion that the
performance of the compressed MC set is independent of the degree of
non-Gaussianity.

%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.49\textwidth]{SM_PDF2/plots/estimators_normal}\\
\includegraphics[width=0.49\textwidth]{SM_PDF2/plots/estimators_MCH}
\includegraphics[width=0.49\textwidth]{SM_PDF2/plots/estimators_CMC}
\end{center}
\vspace{-0.3cm}
\caption{\small 
  \label{fig:estimators} Scatter plot of indicators of deviation from
  gaussianity for all the cross-sections of Table~\ref{tab:processes_H}.
  %
  For each observable, the shift between the median and the mean
  Eq.~(\ref{eq:meanshift}) is shown
  in the horizontal axis, while the shift between standard deviation
  and the 68\% interval Eq.~(\ref{eq:stdshift}) is represented on the vertical
  axis.
%
  The color code shows the KL divergence between the prior and
  either a
  Gaussian (top) or the two reduced sets (bottom): Hessian
  (left) and MC (right).
  }
  
\end{figure}
%%%%%%%%%%%%%%%%%%%%

\subsection{Summary and outlook}
%

In this contribution we have performed a systematic comparison of the three reduced
PDF4LHC15 PDF sets with the prior distribution they have been
constructed from, with particular regard to non-Gaussian features, by
comparing predictions for a wide variety of LHC
cross-sections.
%
Our
general conclusion is that the three sets all perform as expected. We
have specifically verified that the   {\tt PDF4LHC15\_nlo\_100}
Hessian set provides generally the most accurate representation of the
mean and standard deviation of the probability distribution, while the
{\tt PDF4LHC15\_nlo\_mc}  and {\tt PDF4LHC15\_nlo\_30} sets are
less accurate though still quite good.
We have also verified that specialized SM-PDF~\cite{Carrazza:2016htc}
sets can give an equally accurate representation, but with a smaller
number of error-sets, at the price of not being suited for all
possible processes, but with the option of combining them with other
more accurate sets.
%
We
have then verified that in the presence of substantial deviations from
Gaussianity, the  {\tt PDF4LHC15\_nlo\_mc} set
is the most accurate.
%
By
providing a breakdown of our comparisons by type of process, we have
verified that both deviations from Gaussianity and loss of accuracy of
the smaller Hessian set are more marked in regions which are sensitive
to poorly known PDFs, such as the anti-quarks at large $x$.

The results for  the $N_{\sigma}\simeq 600$
cross-sections used for the calculations in
Figs.~\ref{fig:kinplots}--\ref{fig:kinplots3} are
available from the link
\begin{center}
\url{http://pcteserver.mi.infn.it/~nnpdf/PDF4LHC15/gall}
\end{center}
from where they can be accessed in HTML, CSV and ODS formats.

\subsection*{Acknowledgements}
S.~C. and S.~F. are supported in part by an Italian PRIN2010 grant and
by a European Investment Bank EIBURS grant.
%
S.~C. is supported by the HICCUP ERC Consolidator grant (614577).
%
S.~F. and Z.~K. are
supported by the Executive Research Agency (REA) of the European
Commission under the Grant Agreement PITN-GA-2012-316704 (HiggsTools).
%
J.~R. is supported by an STFC Rutherford Fellowship
and Grant ST/K005227/1 and ST/M003787/1 and
by an European Research Council Starting Grant {\it ``PDF4BSM''}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

