\section{Construction and phenomenological applications of PDF4LHC parton distributions 
  \texorpdfstring{\protect\footnote{
    J.~Gao, T.-J.~Hou, J.~Huston, P.~M.~Nadolsky, B.~T.~Wang, K.~P.~Xie
  }}{}}

We revisit the construction and application of
combined PDF sets (PDF4LHC15) developed by the PDF4LHC group in
2015. Our focus is on the meta-analysis technique employed in the
construction of the 30-member PDF4LHC15 sets, and  
especially on aspects that were not fully described in the main
PDF4LHC recommendation document. These aspects include  
construction of the 30-member sets at NLO (in addition to NNLO),
extension of the NLO and NNLO sets to low QCD scales, and construction
of such sets for 4 active flavors. In addition, we clarify a point
regarding the calculation of parton luminosity uncertainties at low
mass. Finally, we present a website containing predictions based on
PDF4LHC15 PDFs for some crucial LHC processes. 

\subsection{Introduction}
To simplify applications of parton distribution functions (PDFs) in
several categories of LHC experimental simulations, 
the 2015  recommendations \cite{Butterworth:2015oua} of the PDF4LHC working group
introduce combinations of CT14~\cite{Dulat:2015mca}, MMHT2014~\cite{Harland-Lang:2014zoa}, and NNPDF3.0~\cite{Ball:2014uwa} PDF ensembles,
by utilizing the Monte Carlo (MC) replica
technique \cite{Watt:2012tq}. The central PDF and the uncertainties
of the combined set are derived from the
900 MC replicas of the error PDFs of the above three input
ensembles. As the 900 error PDFs are often too many to be manageable,
they are ``compressed'' into smaller PDF error sets using three reduction techniques
\cite{Gao:2013bia,Carrazza:2015hva,Carrazza:2015aoa}.  Consequently, the final combined PDFs come in
three versions, one with 30 error sets (PDF4LHC15\_30), and the other two
with 100 error sets (PDF4LHC15\_100 and PDF4LHC15\_MC). Two of these, 
PDF4LHC15\_30 and PDF4LHC\_100, are constructed in the form of Hessian
eigenvector sets \cite{Pumplin:2002vw}. The PDF4LHC15\_MC ensemble is
constructed from MC replicas. The central sets are the same in the
900-replica prior as well as in the  \_100, 
\_30, and \_MC ensembles. They are equal to the average of central
sets of CT14, MMHT2014, and NNPDF3.0 ensembles.  
The error sets of the three PDF4LHC15 ensembles are different,
reflecting the specifics of each reduction technique. They are available in
the LHAPDF library \cite{Buckley:2014ana} at NLO and NNLO in QCD coupling
strength $\alpha_s$, with the central value of $\alpha_{s}(M_{Z})$
equal to 0.118, and with additional sets corresponding to the
$\alpha_s$ variations by 0.0015 around the central value.  

The 30-member ensemble is constructed using the meta-parametrization
technique introduced in \cite{Gao:2013bia}.
This contribution describes additional developments in the
30-member ensemble that happened at the time, 
or immediately after, the release of
the original PDF4LHC recommendation document. They include
construction of the PDF4LHC15\_30 ensemble at NLO, extension of
PDF4LHC15\_30 to scales below 8 GeV, and the specialized ensemble with
4 active quark flavors. These features are already incorporated in the LHAPDF distributions. We provide comparisons of PDFs
and parton luminosities and introduce a website \cite{SMUgallery} 
illustrating essential
LHC cross sections computed with the PDF4LHC15 and other ensembles,
and using a variety of QCD programs.  

When deciding on which of the three PDF4LHC sets to use, it is
important to keep in mind that all of them reproduce well the uncertainties of the
900-replica ``prior'' PDF ensemble. 
This prior itself has some uncertainty both in its central value and
especially in the size of the PDF uncertainty itself, reflecting  differences between the
central values and the uncertainty bands of CT14, MMHT2014 and NNPDF3.0, 
which become especially pronounced at very low $x$ and high $x$.  
At moderate $x$ values, contributing to the bulk of
precision physics cross sections at the LHC, the agreement between the
three input PDF sets is often quite better, meaning that the combined
prior and the three reduced ensembles constructed from it are also known well.  
In general, the 30-member ensemble keeps the lowest,
best-known eigenvector sets, and thus provides a slightly lower estimate for
the uncertainty of the 900-replica prior, but one that is known
with higher confidence than the exact uncertainty of the prior set. 
We will demonstrate that, across many practical applications,  
the 30-member error estimates are typically close both to those of the
prior and of the Hessian 100 PDF error set. 

\subsection{QCD scale dependence of the 30-member NLO PDF4LHC ensemble}
The NLO meta-parametrizations 
are constructed in a slightly different manner compared
to the NNLO version. In Ref.~\cite{Gao:2013bia}, we have 
shown that the differences of the numerical
implementation of DGLAP evolution at NNLO in CT10~\cite{Gao:2013xoa}, 
MSTW2008~\cite{Martin:2009iq}, and
NNPDF2.3~\cite{Ball:2012cx} PDFs are
negligible compared to the intrinsic PDF uncertainties.\footnote{CT10 
PDFs use the $x$-space evolution provided by the program HOPPET~\cite{Salam:2008qg}.}   
However, at NLO, the NNPDF2.3 group uses evolution
that neglects some higher-order terms compared to HOPPET,  which can result 
in deviations by up to 1~\% in the small- and large-$x$ regions, 
compared to the evolution used by CT10 and MSTW2008. These
differences in NLO numerical DGLAP evolution, while formally allowed, 
also affect the most recent generation of NLO PDFs, i.e., 
CT14+MMHT2014 vs. NNPDF3.0. When the 900-replica prior ensemble
at NLO is constructed by taking 300 replicas from each of the input CT14,
MMHT14, and NNPDF3.0 ensembles, the implication is that $Q$-scale
dependence of these replicas is not strictly Markovian. 
Probability regions at the low $Q$ scale, as sampled by the MC
replicas, are not exactly preserved by DGLAP evolution to a higher
$Q$ scale. This is in contrast to the consistent DGLAP evolution of a
single input PDF set, which guarantees that the probability/confidence
value associated with a given error set is independent of the $Q$
scale.

\begin{figure}[t]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/xfvsQ_PDF4LHC15_x1p-3-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/xfvsQ_PDF4LHC15_x2p-1-eps-converted-to}
\caption{The singlet and gluon PDFs, $\Sigma(x,Q)$ and $g(x,Q)$, from 100-
and 30-member PDF4LHC15 sets at NLO and NNLO, plotted vs. the QCD
scale $Q$ at $x=10^{-3}$ (left) and $0.2$ (right).\label{fig:SingletGluonVsQ}}
\end{figure}

Thus, the NLO prior ensemble is not inherently
consistent, even though the deviations in DGLAP evolution of
individual replicas are arguably small. One should apply 
a correction to restore the Markovian nature 
of the evolution. In the PDF4LHC15\_30 NLO set we do this by first
constructing the central PDF set at any $Q$ by averaging the CT14,
MMHT14, and NNPDF3.0 central sets that were evolved by their own
native programs. Then, we reduce the 900-member into the 30-member set
at scale $Q_0=8$ GeV and evolve all replicas to other $Q$ values using
HOPPET. Finally, we estimate the difference between the HOPPET
evolution and
native evolution of the central set, and subtract this difference at
every $Q$ from the HOPPET-evolved values of every error set. After such
universal shift, the $Q$ dependence of all error sets is
practically the same as the native evolution of the central PDF. The
probability regions are now independent of $Q$; this preserves
sum rules for momentum and quark quantum numbers. 

\subsection{PDF4LHC15\_30 PDFs at low $Q$}

\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/err_PDF4LHC15_ifl0_nnlo_q1p4-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/err_PDF4LHC15_ifl1_nnlo_q1p4-eps-converted-to}\\[4mm]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/err_PDF4LHC15_ifl-2_nnlo_q1p4-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/err_PDF4LHC15_ifl-3_nnlo_q1p4-eps-converted-to}
\caption{
PDF central predictions and uncertainty bands for select parton flavors
from the 100- and 30-member NNLO PDF4LHC15 ensembles, plotted versus
$x$ at a QCD scale $Q=1.4$ GeV as ratios to the central PDF4LHC15\_100
distributions.\label{fig:NNLO100and30Vsx}}
\end{figure}


The original formulation of the meta-PDFs had a minimum $Q$ value of 8
GeV. The relatively high lower cutoff on $Q$ was introduced to justify 
the combination of PDFs obtained in different heavy-quark
schemes, and it is sufficient to describe all high-$Q^2$ physics at
the LHC. However, the extension of the \_30 PDFs
down to lower $Q$ values can be useful, too
%JH,  when 
%some loss of accuracy at $Q < 8$ GeV is tolerated, 
as for
example in the simulatation of parton showers and the underlying event in
Monte-Carlo showering programs. The PDF4LHC15\_30 version on LHAPDF includes 
such an extension down to a $Q$ value of 1.4 GeV, obtained by backward
evolution from 8 GeV using HOPPET. It should be remembered
that the PDF4LHC15 combination is statistically consistent when
the factorization scale in the PDFs is much higher than the bottom
mass, as is typical in the bulk of LHC applications. The 
extension below $Q=8$ GeV should be used in less accurate aspects of the
calculation that are not sensitive to heavy-quark mass effects, 
such as inside the parton shower merged onto an (N)NLO fixed-order cross section. 


\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/err_PDF4LHC15_ifl0_nlo_q1p4-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/err_PDF4LHC15_ifl1_nlo_q1p4-eps-converted-to}\\[4mm]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/err_PDF4LHC15_ifl-2_nlo_q1p4-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/err_PDF4LHC15_ifl-3_nlo_q1p4-eps-converted-to}
\caption{Same as Fig.~\ref{fig:NNLO100and30Vsx}, for NLO PDF sets. \label{fig:NLO100and30Vsx}}
\end{figure}

Figure~\ref{fig:SingletGluonVsQ} illustrates the $Q$ dependence of singlet
and gluon PDFs of the \_30 and \_100 ensembles at NLO and NNLO, for
two select values of Bjorken $x$. Figs.~\ref{fig:NNLO100and30Vsx} and
\ref{fig:NLO100and30Vsx} compare the uncertainty bands 
for the $g,u,\bar{d}$ and $\bar{s}$ distributions
at a $Q$ value of 1.4 GeV, at NNLO and NLO, respectively, for the
PDF4LHC\_30 and PDF4LHC\_100 PDF sets. Good agreement between the two
sets is found in all cases; the backward evolution is smooth and
stable across the covered $Q$ range, with only minor deviations observed
below 2 GeV. [When examining the figures, recall
  that the \_30 error bands can be slightly narrower for unconstrained
  $x$ regions and PDF flavors at any $Q$].  


\subsection{PDF4LHC15 parton luminosities at NLO and NNLO}

\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/gglumi13_pdf4lhc3_nnlo_sym-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/qqblumi13_pdf4lhc3_nnlo_sym-eps-converted-to}\\[4mm]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/gglumi13_pdf4lhc3_nlo_sym-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/qqblumi13_pdf4lhc3_nlo_sym-eps-converted-to}
\caption{PDF4LHC15 NNLO and NLO parton luminosities at $\sqrt{s}=13$ TeV in the experimentally accessible rapidity region $|y|<5$. \label{fig:lumi}}
\end{figure}

\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/gglumi13_ct14mmhtnn_nnlo_sym-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/qqblumi13_ct14mmhtnn_nnlo_sym-eps-converted-to}\\[4mm]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/gglumi13_ct14mmhtnn_nlo_sym-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/qqblumi13_ct14mmhtnn_nlo_sym-eps-converted-to}
\caption{NNLO and NLO parton luminosities for PDF4LHC15\_100, CT14, MMHT14, and NNPDF3.0 ensembles at $\sqrt{s}=13$ TeV in the experimentally accessible rapidity region $|y|<5$. \label{fig:lumi2}}
\end{figure}



Even more relevant for physics applications than the PDF error bands  are the parton luminosities. We have calculated the luminosities as a function of the mass of the final state, 
for a center-of-mass energy of
13 TeV. Comparisons of the $gg$ and $q\overline{q}$ PDF luminosities,
at NLO and NNLO, and defined as in \cite{Campbell:2006wx}, are
shown in Fig.~\ref{fig:lumi} for PDF4LHC15\_100, \_30, and \_MC sets, and 
in Fig.~\ref{fig:lumi} for PDF4LHC15\_100, CT14, MMHT14, and NNPDF3.0 sets. 
Note that the size of the uncertainties shown here,
and the level of agreement among the error bands, are 
different at low mass from those
shown in the PDF4LHC document \cite{Butterworth:2015oua}. That is
because, in our plots, a restriction has been 
applied on the $x$ values of the PDFs to
correspond to a rapidity cut of $|y| < 5$ on the produced state. 
Without such a cut, the luminosity integral at masses below 40 GeV receives
contributions from extremely low $x$ of less than $10^{-5}$, 
where (a) the uncertainties are
larger, (b) the LHAPDF grids provided for the 30 PDF sets are outside of
their tabulated range, and (c) the final state is produced in the
forward region outside of the experimental acceptance of the LHC
detectors. Without the constraints on the $x$ range, the
comparisons of parton luminosities at low mass 
are less relevant to LHC measurements. 

\subsection{4-flavor PDF4LHC15\_30 sets}

The nominal \_30 ensemble has been generated for a maximum number of
quark flavors of up to $N_f=5$. An alternative \_30 ensemble
have been now provided for a maximum quark flavors of $N_f=4$ at NLO,
based on the same prescription as for the $N_f=5$ sets, except that 
they are combined at an initial scale of 1.4 GeV in order to avoid 
backward evolution. We choose $\alpha_S(M_Z, N_f=4)=0.1126$ based on 
matching to  $\alpha_S(M_Z, N_f=5)=0.118$ with a
pole mass of 4.56 GeV for the bottom quark (equal to the average
of masses of 4.75 and 4.18 GeV from the CT14, MMHT14, and NNPDF3.0
ensembles, and consistent with the PDG pole mass value). 

\subsection{PDF4LHC15 predictions for QCD observables}
\begin{table}
\begin{center}
\begin{tabular}{|c|c|c|}\hline
 Process          &   Order  &   Type of calculation\\
 \hline\hline
 $p + p \to Z  + X $ &         NLO &    {\sc aMCFast/APPLgrid}\\
 $p + p \to W^+ + X $ &         NLO &     {\sc aMCFast/APPLgrid}    \\
 $p + p \to W^- + X $ &         NLO &     {\sc aMCFast/APPLgrid}    \\
 $p + p \to W + X $, $A_{ch,W}$ &         NLO &     {\sc aMCFast/APPLgrid}    \\
 CMS $p + p \to W(l\nu) + X $, $A_{ch,l}$ &         NLO &     {\sc aMCFast/APPLgrid}    \\
 $p + p \to W^+ \bar{c} + X $ &       NLO &     {\sc aMCFast/APPLgrid}    \\
 $p + p \to W^- c + X $ &      NLO & {\sc aMCFast/APPLgrid}    \\
 $p + p \to t\bar{t} + X $ &         NLO &     {\sc aMCFast/APPLgrid}    \\
 $p + p \to t\bar{t} \gamma\gamma +X $ &         NLO &     {\sc aMCFast/APPLgrid}    \\
 ATLAS inclusive jets   & NLO &     {\sc NLOJET++/APPLgrid}    \\
 ATLAS inclusive dijets & NLO &     {\sc NLOJET++/APPLgrid}    \\
 $p + p \to H(\gamma\gamma) + X $ &          LO, NLO & {\sc MCFM}    \\
 $p + p \to H(\gamma\gamma)+ jet + X $ &     LO, NLO &     {\sc MCFM} \\   
\hline
\end{tabular}
\end{center}
\caption{
Processes, QCD orders, and computer codes employed for comparisons of PDFs
in the online gallery \cite{SMUgallery}.}
\label{tab:Processes}
\end{table}

The PDF4LHC recommendation document \cite{Butterworth:2015oua} 
contains detailed guidelines to help decide which
individual or combined PDFs to use depending on the circumstances. 
%JHWe recommend the PDF user to carefully read the guidelines
%in order to choose the best set(s). 

To assist in this decision, predictions for 
typical LHC QCD observables have been calculated 
for an assortment of PDF sets. In Ref.~\cite{MC2Hgallery}, PDF4LHC15
predictions were made with the {\sc APPLgrid} fast interface
\cite{Carli:2010rw} for published LHC measurements within the fiducial
region. To provide a complementary perspective, 
at a gallery website \cite{SMUgallery}, 
we present LHC cross sections for processes listed 
in Table~\ref{tab:Processes} at 7, 8, and 13 TeV, and computed 
with no or minimal experimental cuts. 
The three (N)NLO ensembles of
the PDF4LHC15 family (\_100, \_30, \_MC \cite{Butterworth:2015oua}) 
are compared to those of 
ABM12 \cite{Alekhin:2013nda}, CT14 \cite{Dulat:2015mca}, 
HERA2.0 \cite{Abramowicz:2015mha},
MMHT14 \cite{Harland-Lang:2014zoa}, and NNPDF3.0 \cite{Ball:2014uwa}.
The cross sections are calculated using (N)LO hard matrix elements either by 
a fast convolution of the PDFs with the tabulated parton-level cross section 
in the {\sc APPLgrid} format \cite{Carli:2010rw}, or by direct Monte-Carlo
integration in {\sc MCFM} \cite{Campbell:2006xx}. 
Default $\alpha_s(M_Z)$ values are used with each PDF set. 
The {\sc APPLgrid} files used in the computations are linked 
to the website. In the {\sc
  APPLgrid} calculations, the hard cross sections are the same for all PDFs,
while the {\sc MCFM}-produced cross sections are sensitive
to Monte-Carlo integration fluctuations that vary depending
on the PDF ensemble, as will be discussed below.


The predictions were computed according to the following procedure. For production 
of $W^{\pm}$, $Z^0$, $t\bar{t}$,
$t\bar{t} \gamma\gamma$, $W^{+}\bar{c}\, (W^{-}c)$, we use
MadGraph\_aMC@NLO \cite{Alwall:2014hca}, 
combined with aMCfast \cite{Bertone:2014zva} to generate {\sc
  APPLgrid} files for different rapidities of the final-state particle. The  
renormalization and factorization scales are 
$\mu_{R}=\mu_{F}=M_W$, $M_Z$, $H_T/2$, $H_T/2$, $M_W$, respectively. $H_T$ is the
scalar sum of transverse masses $\sqrt{p_T^2+m^2}$ of final-state
particles. For $W^{+}\bar{c}\, (W^{-}c)$ production, we neglect small
contributions with initial-state $c$ or $b$ quarks. 
For NLO single-inclusive jet and dijet production, we use public
{\sc APPLgrid} 
files \cite{APPLgrid} in the bins of ATLAS measurements
\cite{Aad:2011fc}, created with the program {\sc NLOJET++}
\cite{Nagy:2001fj,Nagy:2003tz}. Similarly, the $W$ charge asymmetry in CMS experimental
bins \cite{Chatrchyan:2012xt,Chatrchyan:2013mza} is computed with {\sc APPLgrid} 
from \cite{Alekhin:2014irh}.

\begin{figure}[t]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/pdf4lhc100_30_mc_kp_13TeV_graph-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/pdf4lhc100_30_mc_kp_13TeV_ratio_graph-eps-converted-to}
\caption{NLO predictions of $d\sigma/dy(W^{+})$ in the process $pp \to W^{+}\bar{c}$ at the LHC 13 TeV, computed with {\sc APPLgrid}.  \label{fig:Wpc} }
\end{figure}

For cross sections of the Standard Model Higgs boson and Higgs boson+jet
production via gluon fusion, with subsequent decay to $\gamma\gamma$, 
we use {\sc MCFM} in the heavy-top quark approximation. 
Minimal cuts are imposed on the photons; the QCD scales are
$\mu_F=\mu_R=m_H$.

The PDF uncertainties shown are symmetric, computed according to the
prescriptions provided with each PDF ensemble, except for  the HERA2.0
predictions, which are shown with asymmetric uncertainties, including
contributions from both the eigenvector sets and the variation sets.

\begin{figure}[t]
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/pdf4lhc100_30_mc_nlo_kp_13TeV-eps-converted-to}
\hfill
\includegraphics[width=0.47\textwidth]{SM_PDF1/figs/pdf4lhc100_30_mc_nlo_kp_13TeV_ratio-eps-converted-to}
\caption{$d\sigma/dp_{T}(j)$ in the process $gg \to
  H(\gamma \gamma)+jet$ at the LHC 13 TeV, and its relative PDF uncertainties.  \label{fig:gg2hjLO} }
\end{figure}

For each scattering process, our gallery shows plots of differential
cross sections and ratios of PDF uncertainties to the central
prediction based on  PDF4LHC15\_100.  
Figs.~\ref{fig:Wpc} and \ref{fig:gg2hjLO} provide two examples of
comparisons presented on the website. When computed with {\sc
  APPLgrid}, the cross sections reflect genuine differences in the
PDFs; the hard cross sections are the same with all PDF sets. Thus we
observe, for instance, in $W^+\bar c$ production in 
Fig.~\ref{fig:Wpc} that the uncertainties of 
\_100, \_30, and \_MC ensembles are very close across the
central-rapidity range for most processes, with the \_30 uncertainty
being only slightly smaller (as expected), and with the differences
that can be nearly always eliminated by slightly 
scaling the \_30 uncertainty up by a constant factor (e.g.,
by multiplying it by $\approx 1.05$ in Fig.~\ref{fig:Wpc}). 
The differences between the PDF4LHC15 ensembles grow at
rapidities above 2-3, where the cross sections also are rapidly decreasing. The PDF
uncertainties fluctuate more in the forward regions, reflecting
paucity of experimental constraints on the PDFs. 

Another perspective is glanced from 
$H$ and $H+\mbox{jet}$ production cross sections calculated by {\sc
  MCFM}, cf.~Fig.~\ref{fig:gg2hjLO}. [Additional comparisons can be
  viewed on the website.] These
illustrate that often the differences between the PDF4LHC15 reduced
ensembles will be washed out by Monte-Carlo integration errors, save
exceptionally precise calculations. To start, although the LHAPDF
grids for the \_100, \_30, and \_MC {\it central} sets are just independent
tabulations of the {\it same} prior central set
(they are equivalent up to roundoff errors), they will produce different
fluctuations during the Monte-Carlo integration in MCFM or alike program. 
This is exemplified in the right frame in
Fig.~\ref{fig:gg2hjLO}, where the Higgs boson production cross sections are
slightly different for the three LHAPDF tabulations of the central
set solely because of MC fluctuations. In this figure, the cross
sections were evaluated with $10^6$ Monte-Carlo samplings and with PDF
reweighting of events turned on. The events are exactly
the same for all PDF sets within a given ensemble,
and the event sequences are not the same among the ensembles because
of the different roundoffs of the central LHAPDF grids. Even when the event
reweighting is on, the PDF error bands fluctuate together with
their respective central predictions. 


 The MCFM example touches on broader questions. The MC
 fluctuations can be suppressed by increasing the number of events or
 by using coarser binning for the cross sections. These adjustments 
  tend to either lengthen the calculations, especially with the \_100-replica
 ensembles, or to wash out the already small differences between the
 three PDF4LHC15 ensembles. There are several ways for ``averaging'' the input
 central PDF sets, e.g., because they use different evolution codes or
 round-offs. Each of these will lead to a different pattern of MC
 fluctuations.  Finally, if the MC integration is done without PDF event 
 reweighting, MC fluctuations will vary independently
 replica-by-replica. Using the combined PDF4LHC ensemble with fewer
 members may turn out to be preferable in such situations.
 
\subsection*{Acknowledgements}
Work at ANL is supported in part by the U.S. Department of Energy
under Contract No. DE-AC02-06CH11357. Work at SMU was supported
by the U.S. Department of Energy under Grant DE-SC0010129. Work at MSU was supported by
the National Science Foundation under Grant PHY-1410972.

